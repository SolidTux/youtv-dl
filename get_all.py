#!/usr/bin/env python3

import requests
import json
import sys
import re
import os
import errno

email = sys.argv[1]
password = sys.argv[2]
download_folder = sys.argv[3]

login_url = 'https://www.youtv.de/login'
recordings_url = 'https://www.youtv.de/api/v2/recs.json'
show_url = 'https://www.youtv.de/api/v2/recordings/%s.json'

data = {'session[email]': email,
        'session[password]': password}


def replace_special(str):
    s = re.sub(r'\s', '_', str)
    s = s.replace('ä', 'ae')
    s = s.replace('ö', 'oe')
    s = s.replace('ü', 'ue')
    s = s.replace('ß', 'ss')
    s = s.replace('Ä', 'AE')
    s = s.replace('Ö', 'OE')
    s = s.replace('Ü', 'UE')
    s = re.sub(r'[^a-zA-Z0-9-_]', '', s)
    s = re.sub(r'_+', '_', s)
    return s


def download_file(url, filename):
    r = requests.get(url, stream=True)
    f = open(filename, 'wb')
    for chunk in r.iter_content(chunk_size=1024):
        if chunk:
            f.write(chunk)

s = requests.session()
s.post(login_url, data)
resp = s.get(recordings_url)

recordings = json.loads(resp.text)['recordings']
for r in recordings:
    if r['status'] == 'recorded':
        resp = s.get(show_url % r['id'])
        show = json.loads(resp.text)['recording']
        files = show['files']
        max_size = 0
        download_url = ''
        for f in files:
            if f['size'] > max_size:
                max_size = f['size']
                download_url = f['file']
        filename = None
        if (show['series_number'] is not None) and (show['series_season'] is not None) and (show['title'] is not None) and (show['subtitle'] is not None):
            filename = '%s/%s/%s/%03d/%03d-%s' % (
                download_folder,
                replace_special(show['genre']['name']),
                replace_special(show['title']),
                show['series_season'],
                show['series_number'],
                replace_special(show['subtitle']),
            )
        elif (show['series_number'] is not None) and (show['title'] is not None) and (show['subtitle'] is not None):
            filename = '%s/%s/%s/%03d-%s' % (
                download_folder,
                replace_special(show['genre']['name']),
                replace_special(show['title']),
                show['series_number'],
                replace_special(show['subtitle']),
            )
        elif (show['series_season'] is not None) and (show['title'] is not None) and (show['subtitle'] is not None):
            filename = '%s/%s/%s/%03d/%s' % (
                download_folder,
                replace_special(show['genre']['name']),
                replace_special(show['title']),
                show['series_season'],
                replace_special(show['subtitle']),
            )
        elif (show['title'] is not None) and (show['subtitle'] is not None):
            filename = '%s/%s/%s/%s' % (
                download_folder,
                replace_special(show['genre']['name']),
                replace_special(show['title']),
                replace_special(show['subtitle']),
            )
        elif (show['title'] is not None):
            filename = '%s/%s/%s' % (
                download_folder,
                replace_special(show['genre']['name']),
                replace_special(show['title']),
            )
        else:
            filename = '%s/%s/%s/%d' % (
                download_folder,
                replace_special(show['genre']['name']),
                'unknown',
                show['id'],
            )
        try:
            if not os.path.isfile(filename + '.mp4'):
                if not os.path.exists(os.path.dirname(filename + '.mp4')):
                    try:
                        os.makedirs(os.path.dirname(filename + '.mp4'))
                    except OSError as exc:  # Guard against race condition
                        if exc.errno != errno.EEXIST:
                            raise
                print('Downloading %s to %s.' % (download_url, filename + '.mp4'))
                json_out = open(filename + '.json', 'w')
                json.dump(r, json_out)
                download_file(download_url, filename + '.mp4')
            #else:
            #    print('Deleting %s.' % r['id'])
            #    s.delete(show_url % r['id'])
        except Exception as e:
            print('Exception occured, deleting downloaded files.')
            os.remove(filename + '.mp4')
            os.remove(filename + '.json')
            print(e, sys.stderr)
